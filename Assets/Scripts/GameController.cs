﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject[] coches;
    public Text fin;
    public CarController car;

    // Start is called before the first frame update
    void Start()
    {

       
             car = FindObjectOfType<CarController>();


        //Ifs para mirar que coche instanciar despues del menu de seleccion
        if (SeleccionCoche.index == 1)
        {
            Instantiate(coches[0],new Vector3(22f, 15f, 219.9875f),Quaternion.identity);
        }
        else if (SeleccionCoche.index == 2)
        {
            Instantiate(coches[1], new Vector3(22f, 15f, 219.9875f), Quaternion.identity);
        }
       else if (SeleccionCoche.index == 3)
        {
            Instantiate(coches[2], new Vector3(22f, 15f, 219.9875f), Quaternion.identity);
        }

    }

    // Update is called once per frame
    void Update()
    {
        car = FindObjectOfType<CarController>();
        
        //Mensaje de final de partida al pasar la meta(La cual esta oculta en el mapa)
        if (car.meta)
        {
      
            fin.gameObject.SetActive(true);
        }


    }
}
