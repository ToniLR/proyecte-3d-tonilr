﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorTiempo : MonoBehaviour
{
    public Text textoTiempo;
     float tiempo;

    // Start is called before the first frame update
    void Start()
    {
        tiempo = Time.time;
        textoTiempo.text = "Tiempo: 00:00";
    }

    // Update is called once per frame
    void Update()
    {
        //Script para marcador de tiempo In game 
        float tiempoTranscurrido = Time.time - tiempo;
        string mins = ((int)tiempoTranscurrido / 60).ToString("00");
        string segs = (tiempoTranscurrido % 60).ToString("00");
        string horas = ((int)(tiempoTranscurrido / 60)/60).ToString("00");

        string tiempoString = "Tiempo: "+string.Format("{00}:{01}", mins, segs);

        textoTiempo.text = tiempoString.ToString();
    }
}
