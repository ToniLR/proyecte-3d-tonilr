﻿using UnityEngine;
using System.Collections;

public class CamaraFollow : MonoBehaviour
{
    public Transform target;
    public Rigidbody targetGO;
    public float distancia = 3.0f;
    public float altura = 3.0f;
    public float damping = 5.0f;
    public bool smoothRotation = true;
    public bool seguirDetras = true;
    public float rotationDamping = 10.0f;
    public float vel;
    CarController car;

    private void Start()
    {
        targetGO = FindObjectOfType<Rigidbody>();
        target = targetGO.transform;

    }
    void FixedUpdate()
    {
        targetGO = FindObjectOfType<Rigidbody>();
        target = targetGO.transform;
      
        Vector3 wantedPosition;
        //El vector wantedPosition cambia dependiendo del bool seguirDetras
        if (seguirDetras) { 
            wantedPosition = target.TransformPoint(0, altura, -distancia);
        }
        else { 
            wantedPosition = target.TransformPoint(0, altura, distancia);

        }

        //La posicion de la camara y su movimiento 
    transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * damping);

        //Rotacion
        if (smoothRotation)
        {
            
            Quaternion wantedRotation = Quaternion.LookRotation(target.position - transform.position, target.up);
         
            transform.rotation = Quaternion.Slerp(transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
        }
        else { transform.LookAt(target, target.up); }

   
    }
}