﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLuces : MonoBehaviour
{

    public Light luz;
    public bool luces;
    // Start is called before the first frame update
    void Start()
    {
        luces = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Activar Luces con F
        if (Input.GetKeyDown("f") && !luces)
        {
            luz.intensity = 15;
            luces = true;

        }
        //Desactivar luces con F
        else if (Input.GetKeyDown("f") && luces)
        {
            luz.intensity = 0;
            luces = false;

        }
    }
}
