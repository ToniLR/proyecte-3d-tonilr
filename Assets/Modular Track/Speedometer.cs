﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Speedometer : MonoBehaviour
{
    public Rigidbody coche;

    public float maxVel = 0.0f; // La velocidad maxima del coche 

    public float minAnguloFlecha;// el angulo minimo de la flecha en el velocimetro (cuando esta en el 0KM/H)
    public float maxAnguloFlecha;// el angulo maximo de la flecha en el velocimetro(cuando esta en el 260KM/H)

    [Header("UI")]
    public Text textoVel; // El texto que indica la velocidad en el velocimetro;
    public RectTransform flecha; // La flecha del velocimetro

    
    public float vel = 0.0f;

    private void Start()
    {
        coche = FindObjectOfType<Rigidbody>();
    }

    private void Update()
    {
        coche = FindObjectOfType<Rigidbody>();
        //3.6 para pasarlo a kilometros
        vel = coche.velocity.magnitude * 3.6f;

        if (textoVel != null)
            textoVel.text = ((int)vel) + " km/h";
        if (flecha != null)
            flecha.localEulerAngles =
                new Vector3(0, 0, Mathf.Lerp(minAnguloFlecha, maxAnguloFlecha, vel / maxVel));
    }
}
