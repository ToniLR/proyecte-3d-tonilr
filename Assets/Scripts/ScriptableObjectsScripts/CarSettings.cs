﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Coche", menuName = "ScriptableObjects/Coche", order = 1)]
public class CarSettings : ScriptableObject
{
    public float maxTorque;
    public float maxBrakeTorque;
    public float forwardStifness;
    public float sidewaysStifness;
    public int mass;
    public float MaxNitro;
}
