﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeNitro : MonoBehaviour
{
    private Image barra;

    private float currentNitro;
    public float MaxNitro;
    public CarSettings settings;
    CarController car;



    // Start is called before the first frame update
    void Start()
    {
        this.MaxNitro = settings.MaxNitro;
       
        barra = GetComponent<Image>();
        car = FindObjectOfType<CarController>();
    }

    // Update is called once per frame
    void Update()
    {
        //Mueve el valor fillAmount de la imagen de la barra de nitro
        car = FindObjectOfType<CarController>();
        currentNitro = car.nitro;
        barra.fillAmount = currentNitro / MaxNitro;
    }
}
