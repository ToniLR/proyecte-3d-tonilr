﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
public class CarController : MonoBehaviour
{
    public WheelCollider WheelFL;
    public WheelCollider WheelFR;
    public WheelCollider WheelRL;
    public WheelCollider WheelRR;
    public Transform WheelFLtrans;
    public Transform WheelFRtrans;
    public Transform WheelRLtrans;
    public Transform WheelRRtrans;
    public Vector3 eulertest;
    float maxFwdSpeed = -3000;
    float maxBwdSpeed = 1000f;
    float gravity = 9.8f;
    private bool frenado = false;
    private Rigidbody rb;
    public Transform centreofmass;
    public CarSettings settings;
    public float nitro;
    public bool nt;
    public float cd;
    public bool active;
    public  bool meta;
 
    public ParticleSystem boost;
        

    void Start()
    {
        boost.Stop();
       
        meta = false;
      
        //aplica un rigidbody al centro de masa del coche 
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centreofmass.transform.localPosition;
        CargarSettings();
    }

    void FixedUpdate()
    {
        //if para qeu no se mueva el coche cuando llega a la meta
        if (!meta) { 

        if (!frenado)
        {
            WheelFL.brakeTorque = 0;
            WheelFR.brakeTorque = 0;
            WheelRL.brakeTorque = 0;
            WheelRR.brakeTorque = 0;
        }

       //Movimiento del coche cuando se le aplique movimiento por input
        WheelRR.motorTorque = settings.maxTorque * Input.GetAxis("Vertical");
        WheelRL.motorTorque = settings.maxTorque * Input.GetAxis("Vertical");

        //Cambiar la direccion del coche  
        WheelFL.steerAngle = 30 * (Input.GetAxis("Horizontal"));
        WheelFR.steerAngle = 30 * Input.GetAxis("Horizontal");
        }
        else
        {

            WheelRL.brakeTorque = settings.maxBrakeTorque * 20;//0000;
            WheelRR.brakeTorque = settings.maxBrakeTorque * 20;//0000;
            WheelRL.motorTorque = 0;
            WheelRR.motorTorque = 0;
        }
    }
    void Update()
    {
        //if para que no se mueva el coche cuando llega a la meta
        if (!meta) { 
        HandBrake();


        ConfigNitro();

        //for tyre rotate
        WheelFLtrans.Rotate(WheelFL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        WheelFRtrans.Rotate(WheelFR.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        WheelRLtrans.Rotate(WheelRL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        WheelRRtrans.Rotate(WheelRL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        //changing tyre direction
        Vector3 temp = WheelFLtrans.localEulerAngles;
        Vector3 temp1 = WheelFRtrans.localEulerAngles;
        temp.y = WheelFL.steerAngle - (WheelFLtrans.localEulerAngles.z);
        WheelFLtrans.localEulerAngles = temp;
        temp1.y = WheelFR.steerAngle - WheelFRtrans.localEulerAngles.z;
        WheelFRtrans.localEulerAngles = temp1;
        eulertest = WheelFLtrans.localEulerAngles;
        }
        else
        {
            WheelRL.brakeTorque = settings.maxBrakeTorque * 20;//0000;
            WheelRR.brakeTorque = settings.maxBrakeTorque * 20;//0000;
            WheelRL.motorTorque = 0;
            WheelRR.motorTorque = 0;
        }
    }

    /**
     * Freno de mano
     */
    void HandBrake()
    {
        
        if (Input.GetButton("Jump"))
        {
            frenado = true;
        }
        else
        {
            frenado = false;
        }
        if (frenado)
        {

            WheelRL.brakeTorque = settings.maxBrakeTorque * 20;//0000;
            WheelRR.brakeTorque = settings.maxBrakeTorque * 20;//0000;
            WheelRL.motorTorque = 0;
            WheelRR.motorTorque = 0;
        }
    }
    /**
     * Funcion para cargar y  hacer funcionar el Nitro del coche 
     * */
    public void ConfigNitro()
    {
        cd = 0;

        //si el nitro no esta activo
        if (!active)
        {
            nitro+= Time.deltaTime;
        }
        
        //Si nitro es mas grande que el valor maximo lo iguala al valor max
        if (nitro > settings.MaxNitro)
        {
            nitro = settings.MaxNitro;

        }

        //Activa la posibilidad de usar el nitro cuando tengas la mitad del total
        if (nitro >= settings.MaxNitro/2)
        {
            //bool nitro activo
            nt = true;
        }
        //Aplicar Boost
        if (Input.GetKeyDown("e") && nt)
        {
           
          
            //CAMBIAR LOS STIFFNESS DE FORWARD FRICTION 
            //WheelFL forwardFriction Stiffness
            WheelFrictionCurve friction = this.WheelFL.forwardFriction;
            friction.stiffness = 3f;
            this.WheelFL.forwardFriction = friction;
            //WheelFR forwardFriction Stiffness
            friction = this.WheelFR.forwardFriction;
            friction.stiffness = 3f;
            this.WheelFR.forwardFriction = friction;
            //WheelRL forwardFriction Stiffness
            friction = this.WheelRL.forwardFriction;
            friction.stiffness = 3f;
            this.WheelRL.forwardFriction = friction;
            //WheelRR forwardFriction Stiffness
            friction = this.WheelRR.forwardFriction;
            friction.stiffness = 3f;
            this.WheelRR.forwardFriction = friction;
                nitro = 0;
            active = true;

            //bool nitro no activo
            nt = false;

            //Activa Particle system
            boost.Play();   
        }
        //si esta activo tiene un tiempo de duracion 
        if (active)
        {
            cd += Time.deltaTime;
       
            
        }
        if(cd > 0.013)
        {
            active = false;
            //vuelve los valores normales del coche
            CargarSettings();
            boost.Stop();
        }
   
        


    }

    /**
     * Funcion para cargar todas las variables del ScriptableObject a los valores del coche
     * */
    public void CargarSettings()
    {
        this.rb.mass = settings.mass;

        //CAMBIAR LOS STIFFNESS DE FORWARD FRICTION 
        //WheelFL forwardFriction Stiffness
        WheelFrictionCurve friction = this.WheelFL.forwardFriction;
        friction.stiffness = settings.forwardStifness;
        this.WheelFL.forwardFriction = friction;
        //WheelFR forwardFriction Stiffness
        friction = this.WheelFR.forwardFriction;
        friction.stiffness = settings.forwardStifness;
        this.WheelFR.forwardFriction = friction;
        //WheelRL forwardFriction Stiffness
        friction = this.WheelRL.forwardFriction;
        friction.stiffness = settings.forwardStifness;
        this.WheelRL.forwardFriction = friction;
        //WheelRR forwardFriction Stiffness
        friction = this.WheelRR.forwardFriction;
        friction.stiffness = settings.forwardStifness;
        this.WheelRR.forwardFriction = friction;

        //Cambiar el valor stiffness de los sideways 
        //WheelFL sidewaysFriction Stiffness
        friction = this.WheelFL.sidewaysFriction;
        friction.stiffness = settings.sidewaysStifness;
        this.WheelFL.sidewaysFriction = friction;
        //WheelFR sidewaysFriction Stiffness
        friction = this.WheelFR.sidewaysFriction;
        friction.stiffness = settings.sidewaysStifness;
        this.WheelFR.sidewaysFriction = friction;
        //WheelRL sidewaysFriction Stiffness
        friction = this.WheelRL.sidewaysFriction;
        friction.stiffness = settings.sidewaysStifness;
        this.WheelRL.sidewaysFriction = friction;
        //WheelRR sidewaysFriction Stiffness
        friction = this.WheelRR.sidewaysFriction;
        friction.stiffness = settings.sidewaysStifness;
        this.WheelRR.sidewaysFriction = friction;



    }

    //Colision con la meta
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "meta")
        {

            meta = true;
          
        }
    }
}